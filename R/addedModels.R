
#################################################################################################
#### Models #####################################################################################
#################################################################################################

#### Linear nu-SVM using e1071 ####
svmLinear2nu <- caret::getModelInfo("svmLinear2")[[1]]
svmLinear2nu$parameters <- rbind(svmLinear2nu$parameters,data.frame(parameter = "nu",class = "numeric",label = "Nu"))
svmLinear2nu$fit <- function(x, y, wts, param, lev, last, classProbs, ...) 
{
  if (any(names(list(...)) == "probability") | is.numeric(y)) {
    out <- svm(x = as.matrix(x), y = y, kernel = "linear", 
               cost = param$cost, nu = param$nu, ...)
  }
  else {
    out <- svm(x = as.matrix(x), y = y, kernel = "linear", 
               cost = param$cost, nu = param$nu, probability = classProbs, ...)
  }
  out
}
svmLinear2nu$grid <- function (x, y, len = NULL, search = "grid") 
{
  if (search == "grid") {
    out <- expand.grid(cost = sqrt(2)^seq(-6,-2,length.out = len),nu = 10^seq(-5,-.3,length.out = len))
  }
  else {
    out <- data.frame(cost = 2^runif(len, min = -5, max = 10),nu = 10^runif(len,min = -5,max = -0.1))
  }
  out
}

#' @export
svmLinear2nu <- svmLinear2nu

### Linear eps-SVM using e1071
svmLinear2eps <- caret::getModelInfo("svmLinear2")[[1]]
svmLinear2eps$parameters <- rbind(svmLinear2eps$parameters,data.frame(parameter = "eps",class = "numeric",label = "Epsilon"))
svmLinear2eps$fit <- function(x, y, wts, param, lev, last, classProbs, ...) 
{
  if (any(names(list(...)) == "probability") | is.numeric(y)) {
    out <- svm(x = as.matrix(x), y = y, kernel = "linear", 
               cost = param$cost, epsilon = param$eps, ...)
  }
  else {
    out <- svm(x = as.matrix(x), y = y, kernel = "linear", 
               cost = param$cost, epsilon = param$eps, probability = classProbs, ...)
  }
  out
}
svmLinear2eps$grid <- function (x, y, len = NULL, search = "grid") 
{
  if (search == "grid") {
    out <- expand.grid(cost = sqrt(2)^seq(-6,-2,length.out = len),eps = 10^seq(-5,-.3,length.out = len))
  }
  else {
    out <- data.frame(cost = 2^runif(len, min = -5, max = 10),eps = 10^runif(len,min = -5,max = -0.1))
  }
  out
}

#' @export
svmLinear2eps <- svmLinear2eps

### RBF eps-SVM using e1071
svmRBF <- caret::getModelInfo("svmLinear2")[[1]]
svmRBF$parameters <- rbind(svmRBF$parameters,data.frame(parameter = "gamma",class = "numeric",label = "Gamma"))
svmRBF$fit <- function(x, y, wts, param, lev, last, classProbs, ...) 
{
  if (any(names(list(...)) == "probability") | is.numeric(y)) {
    out <- svm(x = as.matrix(x), y = y, kernel = "radial", 
               cost = param$cost, gamma = param$gamma, ...)
  }
  else {
    out <- svm(x = as.matrix(x), y = y, kernel = "radial", 
               cost = param$cost, gamma = param$gamma, probability = classProbs, ...)
  }
  out
}
svmRBF$grid <- function (x, y, len = NULL, search = "grid") 
{
  if (search == "grid") {
    out <- expand.grid(cost = sqrt(2)^seq(-6,-2,length.out = len),gamma = seq(1,50,length.out = len)/ncol(x))
  }
  else {
    out <- data.frame(cost = 2^runif(len, min = -5, max = 10),eps = 10^runif(len,min = -5,max = -0.1))
  }
  out
}

#' @export
svmRBF <- svmRBF

#################################################################################################
#### Kernels ####################################################################################
#################################################################################################

#' Squared exponential kernel with lengthscale.
#' 
#' This is the squared exponential kernel, or covariance function in Gaussian Process terms, with length scale.
#' The length scale parameter controls the influence of variability in the inputs. Higher values mean that the
#' inputs have to be farther away from each other, in order to differ in their outputs.
#' 
gpSE <- function(sigma = 1,lengthScale = 1){
  rval <- function(x, y = NULL) {
    if (!is.vector(x))
      stop("x must be a vector")
    if (!is.vector(y) && !is.null(y))
      stop("y must a vector")
    if (is.vector(x) && is.null(y)) {
      return(1)
    }
    if (is.vector(x) && is.vector(y)) {
      if (!length(x) == length(y))
        stop("number of dimension must be the same on both data points")
      return(exp(sigma * (2 * crossprod(x, y) - crossprod(x) -
                            crossprod(y)) / lengthScale))
    }
  }
  return(methods::new("kernel", .Data = rval, kpar = list(sigma = sigma, lengthScale = lengthScale)))
}
